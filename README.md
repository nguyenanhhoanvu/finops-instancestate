# FinOps

FinOps is a project aim to target the fleet of EC2 instance which is underutilization or idle for certain period of time to either update instance size, shutdown instance or convert instance license.

The process will be done automatically by the application and generate report to notify user after each run.

This project contains source code and supporting files for a serverless application that you can deploy with the SAM CLI. It includes the following files and folders.

- cloudformation - Code for the project infrastructure.
- events - Invocation events that you can use to invoke the function.
- tests - Unit tests for the application code. 
- lambda - Code for lambda functions for application flow.

The application uses several AWS resources, including EC2, Lambda, S3, CloudWatch, EventBridge, StepFunction, ImageBuilder, DynamoDB, SecurityGroup, Roles. These resources are defined in the `template.yaml` file in folder "cloudformation". You can update the template to add AWS resources through the same deployment process that updates your application code.

## Deploy the FinOps application

The project using build and process integrate with Gitlab-ci which defined in .gitlab-ci.yml with 3 different environments (Production / Stage / Development)

The Gitlab pipeline will generate samconfig.toml in stage "Generate SAM Environment Config" which then will be utilized by build and deploy jobs.

## Cleanup

To delete the FinOps application created, "Stack Teardown" stage was added in development environment added for development process and can only be trigger manually. 

## Resources

For project architecture design, please reference "FinOps Architecture Design" file project folder.
