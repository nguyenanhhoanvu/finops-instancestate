# Lambda function to validate new instance

import boto3
import json
import os
import logging
from decimal import Decimal
from datetime import datetime, timedelta, date
# import requests

# Initialize DynamoDB, ec2 and cloudwatch client
dynamodb = boto3.resource('dynamodb')
ec2 = boto3.client('ec2')
cloudwatch = boto3.client('cloudwatch')
stepfunctions = boto3.client('stepfunctions')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Get table name from env
instance_table_name = os.getenv('FINOPS_INSTANCE_STATUS')
instance_table = dynamodb.Table(instance_table_name)
debug_action = ''

def lambda_handler(event, context):
    try:
        logger.info(f"Event data {event}")
        event_body = event.get('body')
        instance_info = json.loads(event_body)
        logger.info(f'instance_info {instance_info}')
        validate_result = []

        # Update instance status in DynamoDB
        for instance in instance_info:
            db_update_response = update_dynamodb_item(instance['old_instance_id'], instance['old_instance_launch_date'], 'completed')
            logger.info(f"db update status for instance {instance['old_instance_id']}: {db_update_response}")
            update_attr = db_update_response.get('Attributes')
            if update_attr:
                logger.info("Update successful.")
                validate_result.append(instance['new_instance_id'])
            else:
                logger.info("Update failed.")

        if len(validate_result) == len(instance_info):
            return {
                'statusCode': 200,
                'body': 'Successfully downsize instance'
            }
        else:
            logger.error("Update failed.")
            return {
                'statusCode': 400,
                'body': 'Update failed'
            }
       
    except Exception as e:
        logger
        logger.error(f"Error perform {debug_action} into DynamoDB: {e}")
        return {
            'statusCode': 500,
            'body': json.dumps(f'Error inserting item: {str(e)}')
        }

def update_dynamodb_item(instance_id, launch_date, action_status):

    today_string = date.today().strftime("%Y-%m-%d")

    # Update items 'action_status' and 'execution_date' in the table
    response = instance_table.update_item(
        Key={
            'instance_id': instance_id,
            'launch_date': launch_date
        },
        UpdateExpression="SET action_status = :action_status, execution_date = :execution_date",
        ExpressionAttributeValues={
            ':action_status': action_status,
            ':execution_date': today_string
        },
        ReturnValues="UPDATED_NEW"
    )

    return response