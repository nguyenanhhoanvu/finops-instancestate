# Lambda function to suggest which size would be use for current instance
# Get the instance list with cpu_utilization < 65% from DynamoDB
# For starting, current will be lower one size from current size
# if instance current size is t2.small new instance size will be t2.micro
# Update the recommend size to dynamoDB together with target_date

import boto3
import json
import os
import base64
import logging
from decimal import Decimal
from datetime import datetime, date, timedelta
# import requests

# Initialize DynamoDB, ec2, s3 and cloudwatch client
dynamodb = boto3.resource('dynamodb')
ec2 = boto3.client('ec2')
s3 = boto3.client('s3')
cloudwatch = boto3.client('cloudwatch')
stepfunctions = boto3.client('stepfunctions')

# Get today's date
today_date = date.today()

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Get table name from env
instance_table_name = os.getenv('FINOPS_INSTANCE_STATUS')
instance_table = dynamodb.Table(instance_table_name)

# Get bucket name and arn
bucket_name = os.getenv('BUCKET_NAME')
bucket_arn = os.getenv('BUCKET_ARN')

debug_action = ''

instance_list = {
    "t": ["2.nano", "2.micro", "2.small", "2.medium", "2.large", "2.xlarge", "3.nano", "3.micro", "3.small", "3.medium", "3.large"], 
    "a": ["1.medium", "1.large", "1.xlarge", "1.2xlarge", "1.4xlarge", "1.metal"], 
    "c": ["4.large", "4.xlarge", "4.2xlarge", "4.4xlarge", "4.8xlarge", "5.large", "5.xlarge", "5.2xlarge", "5.4xlarge", "5.9xlarge", "5.12xlarge"]
}

def lambda_handler(event, context):
    try:
        logger.info(f"Event data {event}")
        logger.info(f'BUCKET NAME {bucket_name} BUCKET ARN {bucket_arn} TABLE NAME {instance_table_name}')

        # Scan the table with a filter expression for cpu_utilization less than 65%
        response = instance_table.scan(
            FilterExpression=boto3.dynamodb.conditions.Attr('cpu_utilization').lt(65) & boto3.dynamodb.conditions.Attr('instance_state').eq('running')
        )

        # Get the instances from the response
        instances = response.get('Items', [])

        # Log the instances for debugging
        logger.info(f"Retrieved instances: {instances}")

        # Extra logic required to calculate cpu_utilization to properly match with instance type
        # For demo purpose, downgrade one level from current instance type
        update_instances = []
        for instance in instances:
            instance_type = instance["instance_type"]
            recommend_instance_type = (
                instance_type[0] + get_smaller_instance(instance_list[instance_type[0]], instance_type[1:]) 
                    if instance_list[instance_type[0]].index(instance_type[1:]) != 0 
                    else ''
            )

            # # Get possible instance user_data
            # instance_user_data = get_user_data(instance['instance_id'])
            # logger.info(f"instance_user_data {instance_user_data}")

            # Generate new instance list with instance size, target_date and action_status
            if recommend_instance_type:
                update_instances.append({ 
                    "instance_id": instance["instance_id"],
                    "identified_date": today_date.strftime('%Y-%m-%d'),
                    "action_type": "downsize",
                    "recommend_instance_type": recommend_instance_type, 
                    "target_date": today_date.strftime('%Y-%m-%d'),
                })
            
        logger.info(f"update_instances: {update_instances}")

        # Update new instance list to S3 bucket name 'dowsize_instance_list' with instance_id, target_date, recommend_instance_type
        s3_response = s3.put_object(
            Bucket=bucket_name,
            Key='update_instances.json',
            Body=json.dumps(update_instances),
            ContentType='application/json'
        )
        
        print(f"response from s3 {s3_response}")
        return {
            'statusCode': 200,
            'body': {"nextStep": True}
        }
       
    except Exception as e:

        logger.error(f"Error perform {debug_action} into DynamoDB: {e}")
        return {
            'statusCode': 500,
            'body': json.dumps(f'Error: {str(e)}')
        }

def get_smaller_instance(lst, size):
    try:
        index = lst.index(size)
        if index == 0:
            return None
        
        return lst[index - 1]
    
    except ValueError:
        return None
    
# Function to get user data
def get_user_data(instance_id):
    response = ec2.describe_instance_attribute(
        InstanceId=instance_id,
        Attribute='userData'
    )
    user_data = response['UserData']['Value']
    decoded_user_data = base64.b64decode(user_data).decode('utf-8')
    return decoded_user_data
