# Lambda function to monitor instance
# Get instance list that currently has 'running' state in table
# Get the cpu_utilization from cloudwatch statistic metric
# Update the cpu_utilization field from cloudwatch

import boto3
import json
import os
import logging
from decimal import Decimal
from datetime import datetime, timedelta, timezone
# import requests

# Initialize DynamoDB, ec2 and cloudwatch client
dynamodb = boto3.resource('dynamodb')
ec2 = boto3.client('ec2')
cloudwatch = boto3.client('cloudwatch')
stepfunctions = boto3.client('stepfunctions')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Get table name from env
instance_table_name = os.getenv('FINOPS_INSTANCE_STATUS')
instance_table = dynamodb.Table(instance_table_name)
stepfunction_arn = os.getenv('STEPFUNCTION_ARN', '')
debug_action = ''

def lambda_handler(event, context):
    try:
        # Scan the table and filter for running instances
        response = instance_table.scan(
            FilterExpression=boto3.dynamodb.conditions.Attr('instance_state').eq('running')
        )

        running_instances = response.get('Items', [])
        
        # Remove instance that has attr 'identifed_date' less than a week from the list
        identified_ids, unidentifed_instances = filter_unidentified_instances(running_instances)
        logger.info(f'Identified_ids {identified_ids}')
        logger.info(f'unidentifed_instances {unidentifed_instances}')

        instance_ids = [(instance['instance_id'], instance["launch_date"], instance.get("cpu_utilization", 0)) for instance in unidentifed_instances]

        # Default value to trigger step function and possible input data
        # trigger_step_function = False
        input_data = {}

        # Get CPU usage for the past 3 minutes
        for instance in instance_ids:
            # Get the current time and time 5 minutes ago
            current_time = datetime.now(timezone.utc)
            # end_time = current_time - timedelta(minutes=1)
            start_time = current_time - timedelta(hours=1)

            logger.info(f"Current instance_id {instance[0]} with previous cpu_utilization {instance[2]}")
            response = cloudwatch.get_metric_statistics(
                Namespace='AWS/EC2',
                MetricName='CPUUtilization',
                Dimensions=[
                    {
                        'Name': 'InstanceId',
                        'Value': instance[0]
                    },
                ],
                StartTime=start_time,
                EndTime=current_time,
                Period=300, #5 minutes
                Statistics=['Average', 'Maximum']
            )

            logger.info(f"Response from CloudWatch for instance {instance[0]}: {response}")
            # Extract the average CPU usage
            datapoints = response['Datapoints']
            if datapoints:
                average_cpu = Decimal(str(datapoints[0]['Average']))
                logger.info(f"Average CPU usage for instance {instance}: {average_cpu}%")

                # Check if current cpu_utilization is not 0 then update DB
                if average_cpu > instance[2]:
                    # Update cpu_utilization field for instance 
                    update_response = instance_table.update_item(
                        Key={"instance_id": instance[0], "launch_date": instance[1]},
                        UpdateExpression="SET cpu_utilization = :cpu_usage_value",
                        ExpressionAttributeValues={':cpu_usage_value': average_cpu},
                        ReturnValues="UPDATED_NEW"
                    )
                    logger.info(f"update cpu_usage status: {update_response}")
                    input_data.update({
                        instance[0]: {
                            "instance_id": instance[0], 
                            "launch_date": instance[1], 
                            "cpu_utilization": datapoints[0]['Average']
                        }
                    })

            else:
                logger.info(f"No CPU usage data available for instance {instance} skip update")

        logger.info(f"NUMBER OF KEY VAL IN INPUT_DATA: {len(input_data)}")
        logger.info(f"NUMBER OF RUNNING_ISNTANCE: {len(running_instances)}")
        # Trigger stepfunctions when there is instance in input_data        
        if len(input_data):
            # EventBridge trigger lambda to collect cpu_utilization every 3 minutes, skip step function if already run for demo purposes
            # Get the latest execution
            response = stepfunctions.list_executions(
                stateMachineArn=stepfunction_arn,
                statusFilter='SUCCEEDED',  # other statusFilter value: 'FAILED', 'RUNNING', etc.
                maxResults=1
            )
            logger.info(f'Stepfunction response {response}')

            if not response['executions']:
                # Trigger step function code goes here
                logger.info('Stepfunction status not found, TRIGGER STEP FUNCTION!')
                stepfunctions.start_execution(
                    stateMachineArn=stepfunction_arn,
                    input=json.dumps(input_data)
                )
                logger.info(f"SUCCESS TRIGGER STEP FUNCTION")
                return {
                    'statusCode': 200,
                    'body': json.dumps('Successfully update cpu_usage for instance')
                }
            
            # Step function already ran, check if the last run is within 24hrs
            last_execution = response['executions'][0]
            last_execution_time = last_execution['startDate']
            logger.info(f'stepfunction last_execution_time {last_execution_time}')
            # Calculate the time difference
            now = datetime.now(timezone.utc)
            time_difference = now - last_execution_time
            # Check if it has been more than 24 hours since the last run
            if time_difference > datetime.timedelta(hours=24):
                logger.info(f"STEP FUNCTION LAST RUN MORE THAN 24H, START STEPFUNCTION")
                start_response = stepfunctions.start_execution(
                    stateMachineArn=stepfunction_arn,
                    input=json.dumps(input_data)
                )
                logger.info(f"STEP FUNCTION START STATUS {start_response}")
                logger.info(f"input_data for stepfunction {input_data}")

        return {
            'statusCode': 200,
            'body': json.dumps('Successfully update cpu_usage for instance')
        }
       
    except Exception as e:
        logger
        logger.error(f"Error: {e}")
        return {
            'statusCode': 500,
            'body': json.dumps(f'Error inserting item: {str(e)}')
        }

# Function to filter unidentified instance
def filter_unidentified_instances(instances):
    identified_instance_ids = []
    unidentified_instances = []
    for instance in instances:
        if instance.get('identified_date'):
            identified_instance_ids.append(instance['instance_id'])
        else:
            unidentified_instances.append(instance)

    return (identified_instance_ids, unidentified_instances)