# Lambda function to confirm instance for downsizing action
# Check if instance is good to go for downsizing and update status in DynamoDB
# If any changes reflect in file "modified_action" then use the latest status to ongoing process
# etc if instance is scheduled for downsize in 'dowsize_instance_list' and instance is set to cancel in 'modified_action'
# Then there will be no downsize process for that particular instance
# Confirm instance also take target_date into account for calculate action_date for downsizing
# The target date is temporary set to 7 days from identified_date

### Need add Lambda into CloudFormation Template ###

import boto3
import json
import os
import base64
import logging
from decimal import Decimal
from datetime import datetime, timedelta, timezone
# import requests

# Initialize DynamoDB, ec2_client, ec2_resource, s3 and cloudwatch client
dynamodb = boto3.resource('dynamodb')
ec2_client = boto3.client('ec2')
ec2_resource = boto3.resource('ec2')

s3 = boto3.client('s3')
cloudwatch = boto3.client('cloudwatch')
stepfunctions = boto3.client('stepfunctions')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# S3 bucket and object details
bucket_name = os.getenv('BUCKET_NAME')
file_key = 'update_instances.json'

# Get table name from env
instance_table_name = os.getenv('FINOPS_INSTANCE_STATUS')
instance_table = dynamodb.Table(instance_table_name)
debug_action = ''

def lambda_handler(event, context):
    try:
        # Download object from S3 bucket
        s3_response = s3.get_object(Bucket=bucket_name, Key=file_key)

        # Read and decode the JSON content
        json_content = s3_response['Body'].read().decode('utf-8')
        
        json_data = json.loads(json_content)

        for item in json_data:
            logger.info(f"Instance: {item}")

        # Get list of instance with instance_state is 'running' in DynamoDB
        db_response = instance_table.scan(
            FilterExpression=boto3.dynamodb.conditions.Attr('instance_state').eq('running')
        )
        logger.info(f'db_response list {db_response}')
        db_list = db_response.get("Items")

        updated_list = []

        # Create a updated_list that has instance_id in both list and update that instance
        updated_list = update_instance_list(json_data, db_list, updated_list)
        logger.info(f"updated_list {updated_list}")

        ami_list = []

        # Update instance document in DynamoDB with new field 'user_data': boolean, 'action_status': str, 'action_date': str, recommend_size: str 
        for instance in updated_list:
            # remove instance_id and launch_date key from dictionary before perform update
            instance_id = instance.pop('instance_id', None)
            launch_date = instance.pop('launch_date', None)

            # Create an AMI
            current_time_str = datetime.now(timezone.utc).strftime('%Y%m%d%H%M%S')
            image_name = f"AMI-{instance_id}-{current_time_str}"
            ami_response = ec2_client.create_image(
                InstanceId=instance_id,
                Name=image_name,
                NoReboot=True
            )

            # Extract the Image ID
            image_id = ami_response['ImageId']
            logger.info(f"Creating AMI {image_id} for instance {instance_id}")

            # Wait until the image is available
            image = ec2_resource.Image(image_id)
            image.wait_until_exists(Filters=[{
                'Name': 'state',
                'Values': ['available']
            }])
            logger.info(f"AMI {image_id} is now available.")

            # Save the AMI ID to S3
            # Create AMI ID dictionary
            ami_data = {
                'instance_id': instance_id,
                'backup_image_id': image_id,
                'image_name': image_name,
                'created_at': datetime.now(timezone.utc).strftime('%Y-%m-%d:%H:%M:%S')
            }

            # Add AMI dict into list for upload to S3
            ami_list.append(ami_data)

            # Update instance in DynamoDB
            db_update_response = update_dynamodb_item(instance_id, launch_date, instance)
            logger.info(f"db_update_response {db_update_response}")

        s3.put_object(
            Bucket=bucket_name,
            Key='ami-backups/ami_id.json',
            Body=json.dumps(ami_list)
        )
        logger.info(f"AMI ID {image_id} saved to S3 bucket {bucket_name} with key ami-backups/ami_id.json")
        
        return {
            'statusCode': 200,
            'body': json.dumps('Successfully confirm instance for execution process')
        }
       
    except Exception as e:
        logger.error(f"Error: {e}")
        return {
            'statusCode': 500,
            'body': json.dumps(f'Error inserting item: {str(e)}')
        }

def update_instance_list(s3_list, db_list, updated_list):
    logger.info(f"s3_list {s3_list}")
    logger.info(f"db_list {db_list}")
    # Create a dictionary from s3_list for quick lookup by db_list
    s3_dict = {instance['instance_id']: {k: v for k, v in instance.items() if k != 'instance_id'} for instance in s3_list}

    # Iterate over db_list and find matching instances in with s3_dict key and update
    for instance in db_list:
        if instance["instance_id"] in s3_dict:
            instance.update(s3_dict[instance["instance_id"]])
            instance.update({"action_status": "pending"})
            updated_list.append(instance)
            
    return updated_list

def update_dynamodb_item(instance_id, launch_date, update_attr):
    logger.info(f"instance_id to update {instance_id}")
    logger.info(f"launch_date {launch_date}")
    # Define the update expression and attribute values
    update_expression = "SET " + ", ".join(f"#{k} = :{k}" for k in update_attr.keys())
    attribute_names = {f"#{k}": k for k in update_attr.keys()}
    attribute_values = {f":{k}": v for k, v in update_attr.items()}

    # Update the item in the table
    response = instance_table.update_item(
        Key={
            'instance_id': instance_id,
            'launch_date': launch_date
        },
        UpdateExpression=update_expression,
        ExpressionAttributeNames=attribute_names,
        ExpressionAttributeValues=attribute_values,
        ReturnValues="UPDATED_NEW"
    )

    return response