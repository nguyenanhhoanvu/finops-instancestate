# Lambda function to downsize instance

import boto3
import json
import os
import base64
import logging
from decimal import Decimal
from datetime import datetime, date
# import requests

# Initialize DynamoDB, ec2 and cloudwatch client
dynamodb = boto3.resource('dynamodb')
s3 = boto3.client('s3')
ec2 = boto3.client('ec2')
cloudwatch = boto3.client('cloudwatch')
cloudformation = boto3.client('cloudformation')
stepfunctions = boto3.client('stepfunctions')

# S3 bucket and object details
bucket_name = os.getenv('BUCKET_NAME')
file_key = 'ami-backups/ami_id.json'
stack_name = os.getenv('STACK_NAME')
security_output_key = 'SecurityGroupId'

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Get table name from env
instance_table_name = os.getenv('FINOPS_INSTANCE_STATUS')
instance_table = dynamodb.Table(instance_table_name)
debug_action = ''

def lambda_handler(event, context):
    try:
        logger.info(f"Event data {event}")

        # Get instance list with 'action_status' pending
        # Get list of instance with instance_state is 'running' in DynamoDB
        db_response = instance_table.scan(
            FilterExpression=boto3.dynamodb.conditions.Attr('action_status').eq('pending')
        )
        logger.info(f'db_response list {db_response}')
        db_list = db_response.get("Items")

        today_string = date.today().strftime("%Y-%m-%d")
        action_list = [instance for instance in db_list if instance.get("target_date") == today_string]
        logger.info(f'action_list {action_list}')

        # Fetch the json list of ami from S3
        s3_response = s3.get_object(Bucket=bucket_name, Key=file_key)

        # Read and decode the JSON content
        json_content = s3_response['Body'].read().decode('utf-8')
        json_data = json.loads(json_content)

        logger.info(f'json_data from S3 {json_data}')

        # Update action list with ami id from S3
        updated_action_list = update_action_list(action_list, json_data)
        logger.info(f'update_action_list {updated_action_list}')

        # Instance id list
        instance_ids = [instance['instance_id'] for instance in updated_action_list]
        logger.info(f'instance id list {instance_ids}')

        # Stop current instance
        for instance in updated_action_list:
            instance_current_state = get_instance_state(instance['instance_id'])
            if instance_current_state == 'running':
                response = ec2.stop_instances(InstanceIds=[instance['instance_id']])
                logger.info(f'stop instance response {response}')

                # Wait until the instance is in 'stopped' state
                stopped_waiter = ec2.get_waiter('instance_stopped')
                logger.info(f"Waiting for instance {instance['instance_id']} to stop...")
                stopped_waiter.wait(InstanceIds=[instance['instance_id']])
                logger.info(f"Instance {instance['instance_id']} is now stopped.")

        # Terminate all running instances
        ec2.terminate_instances(InstanceIds=instance_ids)
    
        # Wait until all instances are terminated
        waiter = ec2.get_waiter('instance_terminated')
        waiter.wait(InstanceIds=instance_ids)

        # Convert updated_action_list into dict with key is instance_id
        updated_list_dict = {instance['instance_id']: {k: v for k, v in instance.items() if k != 'instance_id'} for instance in updated_action_list}
        logger.info(f'updated_list_dict {updated_list_dict}')

        # Launch new instances with the new instance type and backup AMI
        new_instances_info = []
        for instance_id in instance_ids:
            # Describe the old instance to get details
            old_instance = ec2.describe_instances(InstanceIds=[instance_id])
            old_instance_info = old_instance['Reservations'][0]['Instances'][0]
            logger.info(f'old_instance_info {old_instance_info}')

            # Extract security groups and subnet from the old instance
            # security_groups = [sg['GroupId'] for sg in old_instance_info['SecurityGroups']]
            subnet_id = old_instance_info.get('SubnetId')

            # Retrieve the security group ID
            security_group_id = get_security_group_id(stack_name, security_output_key)
            if not security_group_id:
                raise Exception(f'Security group ID not found in stack {stack_name}')
            logger.info(f'security_group_id {security_group_id}')

            # Check key value before run new instance
            logger.info(
                f"ImageId {updated_list_dict[instance_id]['backup_image_id']}, InstanceType {updated_list_dict[instance_id]['recommend_instance_type']}, SecurityGroupIds {security_group_id},SubnetId {subnet_id}"
            )

            # Setup params for new instance
            run_instances_params = {
                'ImageId': updated_list_dict[instance_id]['backup_image_id'],
                'InstanceType': updated_list_dict[instance_id]['recommend_instance_type'],
                'SecurityGroupIds': [security_group_id],
                'Placement':old_instance_info['Placement'],
                'MinCount': 1,
                'MaxCount': 1
            }

            # Include SubnetId only if it exists
            if subnet_id:
                run_instances_params['SubnetId'] = subnet_id
            tags = []
            tags.append({'Key': 'Project', 'Value': 'finop'})
            if updated_list_dict[instance_id].get('name_tag'):
                logger.info(f"name_tag of instance {updated_list_dict[instance_id]['name_tag']}")
                tags.append({'Key': 'Name', 'Value': updated_list_dict[instance_id]['name_tag']})
            if updated_list_dict[instance_id].get('env_tag'):
                logger.info(f"env_tag of instance {updated_list_dict[instance_id]['env_tag']}")
                tags.append({'Key': 'Environment', 'Value': updated_list_dict[instance_id]['env_tag']})
            run_instances_params['TagSpecifications'] = [
                {
                    'ResourceType': 'instance',
                    'Tags': tags
                }
            ]

            # Get possible instance user_data
            instance_user_data = get_user_data(instance['instance_id'])
            logger.info(f"instance_user_data {instance_user_data}")

            if instance_user_data:
                run_instances_params['UserData'] = instance_user_data

            ec2_run_response = ec2.run_instances(**run_instances_params)

            logger.info(f'ec2_run_response {ec2_run_response}')

            new_instance_id = ec2_run_response['Instances'][0]['InstanceId']
            new_instances_info.append({
                'old_instance_id': instance_id,
                'old_instance_launch_date': updated_list_dict[instance_id]['launch_date'],
                'new_instance_id': new_instance_id
            })

        logger.info(f'new_instances_info {new_instances_info}')

        # Passing new_instance_info to L6 for validating and update status on DynamoDBs
        return {
            'statusCode': 200,
            'body': json.dumps(new_instances_info)
        }
       
    except Exception as e:
        logger
        logger.error(f"Error: {e}")
        return {
            'statusCode': 500,
            'body': json.dumps(f'Error inserting item: {str(e)}')
        }
    
def update_action_list(action_list, ami_list):
    logger.info(f"action_list {action_list}")
    logger.info(f"ami_list {ami_list}")

    # Create a dictionary from ami_list for quick lookup by action_list
    ami_dict = {instance['instance_id']: {k: v for k, v in instance.items() if k != 'instance_id'} for instance in ami_list}
    logger.info(f"ami_dict {ami_dict}")

    # Iterate over action_list and find matching instances in with ami_list key and update
    for instance in action_list:
        if instance["instance_id"] in ami_dict:
            instance.update(ami_dict[instance["instance_id"]])
    logger.info(f"function update_action_list return {action_list}")
    
    return action_list

# Function to get user data
def get_user_data(instance_id):
    response = ec2.describe_instance_attribute(
        InstanceId=instance_id,
        Attribute='userData'
    )
    user_data = response['UserData']['Value']
    decoded_user_data = base64.b64decode(user_data).decode('utf-8')
    return decoded_user_data

# Function to check the state of an instance
def get_instance_state(instance_id):
    response = ec2.describe_instances(InstanceIds=[instance_id])
    state = response['Reservations'][0]['Instances'][0]['State']['Name']
    return state

# Function to get the security group ID from CloudFormation
def get_security_group_id(stack_name, output_key):
    response = cloudformation.describe_stacks(StackName=stack_name)
    for stack in response['Stacks']:
        for output in stack['Outputs']:
            if output['OutputKey'] == output_key:
                return output['OutputValue']
    return None
