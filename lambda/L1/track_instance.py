# Lambda function to tracking instance
# If new instance created with status pending, function add new instance document into dynamodb
# If instance changing status between 'running', 'shutting-down' function will update instance document accordingly
# If instance change status to 'stopping' or 'terminated', function will remove instance from DynamoDB table

import boto3
import json
import os
import logging
from datetime import date
# import requests

# Initialize DynamoDB and ec2 client
dynamodb = boto3.resource('dynamodb')
ec2 = boto3.client('ec2')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Get table name from env
# down_sizing_table_name = os.getenv('FINOPS_DOWNSIZING_INSTANCE')
instance_table_name = os.getenv('FINOPS_INSTANCE_STATUS')
logger.info(f"Table name: {instance_table_name}")
instance_table = dynamodb.Table(instance_table_name)
debug_action = ''

def lambda_handler(event, context):
    try:
        instance_id = event['instance_id']
        instance_state = event['state']
        logger.info(f"Instance_id from EventBridge: {instance_id} instance_state {instance_state}")

        # Describe the instance to get additional details
        ec2_describe = ec2.describe_instances(InstanceIds=[instance_id])
        instance_details = ec2_describe['Reservations'][0]['Instances'][0]
        logger.info(f'Instance Details: {instance_details}')
        tags_dict = {tag['Key']: tag['Value'] for tag in instance_details['Tags']}

        # Add instance document to dynamoDB table if the instance state is 'pending'
        if instance_state == 'pending':
            # Get keys values for adding document
            debug_action = 'put_item'
            instance_type = instance_details['InstanceType']
            availability_zone = instance_details['Placement']['AvailabilityZone']
            image_id = instance_details['ImageId']
            vpc_id = instance_details['VpcId']
            subnet_id = instance_details['SubnetId'] if 'SubnetId' in instance_details else ''
            owner_id = instance_details['NetworkInterfaces'][0]['OwnerId'] if 'OwnerId' in instance_details['NetworkInterfaces'][0] else ''
            name_tag = tags_dict['Name']
            env_tag = tags_dict['Environment']
            instance_state = instance_state
            # tags = instance_details['Tags']
            new_instance = {
                'instance_id': instance_id,
                'instance_state': 'pending',
                'image_id': image_id,
                'instance_type': instance_type,
                'availability_zone': availability_zone,
                'launch_date': date.today().isoformat(), # Convert date to string otherwise will cause error in put_item function
                'name_tag': name_tag,
                'env_tag': env_tag,
                'vpc_id': vpc_id,
                'subnet_id': subnet_id,
                'owner_id': owner_id,
            }
            add_response = instance_table.put_item(Item=new_instance)
            logger.info(f"create record status: {add_response}")
            return {
                'statusCode': 200,
                'body': json.dumps('Successfully adding new instance')
            }

        # Update instance status field in dynamoDB table if the instance state changed
        if instance_state == 'running' or instance_state == 'shutting-down' or instance_state == 'terminated'  or instance_state == 'stopping':
            debug_action = 'update_item'
            logger.info(f"Trigger condition update with instance_state {instance_state}")
            update_response = instance_table.update_item(
                Key={"instance_id": instance_id, "launch_date": instance_details['LaunchTime'].strftime('%Y-%m-%d')},
                UpdateExpression="SET instance_state = :new_status",
                ExpressionAttributeValues={':new_status': instance_state},
                ReturnValues="UPDATED_NEW"
            )
            logger.info(f"Update response: {update_response}")
            return {
                'statusCode': 200,
                'body': json.dumps('Successfully updated instance')
            }

        # # Delete instance from dynamoDB table if the instance state is terminated
        # if instance_state == 'terminated'  or instance_state == 'stopping':
        #     debug_action = 'delete_item'
        #     logger.info(f"Trigger condition delete with instance_state {instance_state}")
        #     delete_response = instance_table.delete_item(Key={"instance_id": instance_id, "launch_date": instance_details['LaunchTime'].strftime('%Y-%m-%d')})
        #     logger.info(f"delete status: {delete_response}")
        #     return {
        #         'statusCode': 200,
        #         'body': json.dumps('Successfully delete instance')
        #     }
    except Exception as e:
        logger
        logger.error(f"Error perform {debug_action} into DynamoDB: {e}")
        return {
            'statusCode': 500,
            'body': json.dumps(f'Error inserting item: {str(e)}')
        }
